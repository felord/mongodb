package cn.felord.mongodb.dao;

import cn.felord.mongodb.base.MongoDao;
import cn.felord.mongodb.entity.FileEntity;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author dax.
 * @version v1.0
 * @since 2018/3/1 14:47
 */
@Component
public class FileEntityDao implements MongoDao<FileEntity> {
    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public void save(FileEntity fileEntity) {
        mongoTemplate.save(fileEntity, "images");
    }

    public FileEntity findByName(String name) {
        Query query = new Query(Criteria.where("originalFilename").is(name));
        return mongoTemplate.findOne(query, FileEntity.class, "images");
    }
}
