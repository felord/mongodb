package cn.felord.mongodb.dao;

import cn.felord.mongodb.base.MongoDao;
import cn.felord.mongodb.entity.Book;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author dax.
 * @version v1.0
 * @since 2018/3/1 14:09
 */
@Component
public class BookDao implements MongoDao<Book> {
   @Resource

   private MongoTemplate mongoTemplate;

    @Override
    public void save(Book book) {
        mongoTemplate.save(book,"books");
    }
}
