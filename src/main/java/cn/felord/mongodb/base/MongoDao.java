package cn.felord.mongodb.base;

/**
 * @author dax.
 * @version v1.0
 * @since 2018/3/1 14:08
 */

public interface MongoDao <T> {

       void save(T t);
}
