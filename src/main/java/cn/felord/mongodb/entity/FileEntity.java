package cn.felord.mongodb.entity;

import java.io.Serializable;

/**
 * @author dax.
 * @version v1.0
 * @since 2018/3/1 14:41
 */

public class FileEntity implements Serializable {

    private static final long serialVersionUID = 5782057957070415211L;
    private String originalFilename;
    private byte[] datas;
    private String contentType;
    private String name;

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public byte[] getDatas() {
        return datas;
    }

    public void setDatas(byte[] datas) {
        this.datas = datas;
    }

    public String getContentType() {
        return contentType;
    }

    public void setConentType(String contentType) {
        this.contentType = contentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
