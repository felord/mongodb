package cn.felord.mongodb.entity;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author dax.
 * @version v1.0
 * @since 2018/3/1 13:43
 */

public class Book implements Serializable {

    private static final long serialVersionUID = 1601067061471209534L;
    @Id
    private String id;

    private String name;

    private String author;

    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", name:" + name +
                ", author:" + author +
                ", type:" + type +
                '}';
    }
}
